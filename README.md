# Snake

The popular Snake game. One of the first javascript games I built. The goal is to eat as many apples without leaving or hitting the body of the snake. Each time you eat an apple, the snake gets longer.

Use arrow keys to change the direction of the snake. Press 's' to start the game and 'p' to pause.

[link][weburl]

[weburl]: https://th11.github.io/snake/snake.html

## Features
- Pause / resume
- Personal high-score (tracked via cookie)
- Increasing difficult
