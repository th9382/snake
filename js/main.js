(function () {
  if (typeof SnakeGame === 'undefined') { window.SnakeGame = {}; }

  var Tile = SnakeGame.Tile = function (row, column) {
    this.row = row;
    this.column = column;
  };

  var Board = SnakeGame.Board = function (dim) {
    this.grid = [];
    this.dim = dim;
    this.buildGrid();
  };

  Board.prototype.buildGrid = function () {
    for( var row = 0; row < this.dim; row++ ) {
      for( var col = 0; col < this.dim; col++ ) {
        var tile = new SnakeGame.Tile(row, col);
        this.grid.push(tile);
      }
    }
  };

  Board.prototype.renderGrid = function (wrapper) {
    for ( var i = 0, n = this.grid.length; i < n; i++ ) {
      var tile = this.grid[i];
      var $tile = $('<div>');
      $tile.addClass('cell');
      $tile.attr('id', 'c_' + tile.row + '_' + tile.column);
      wrapper.append($tile);
    }
  };

  var Food = SnakeGame.Food = function (board) {
    this.board = board;
    this.generate();
  };

  SnakeGame.Food.prototype.generate = function () {
    do {
      this.row = Math.floor( Math.random() * this.board.dim );
      this.column = Math.floor( Math.random() * this.board.dim );
    } while ( $("#c_" + this.row + "_" + this.column).hasClass("snake"));
  };

  var Snake = SnakeGame.Snake = function () {
    this.row = 5;
    this.column = 5;
    this.speed = 200;
    this.body = [[this.row, this.column]];
  };

  Snake.prototype = {
    constructor: Snake,

    resetSpeed: function () {
      this.speed = 200;
    },

    increaseSpeed: function () {
      this.speed *= 0.98;
    },

    move: function (direction) {
      switch(direction) {
        case 40: // down
          this.row++;
          break;
        case 38: // up
          this.row--;
          break;
        case 37: // left
          this.column--;
          break;
        case 39: // right
          this.column++;
          break;
        default:
          return;
        }

      this.body.unshift([this.row, this.column]);
      }
  };

  var Game = SnakeGame.Game = function ($el) {
    this.$el = $el;
    this.$instructions = $(".instructions");
    this.$counter = $("#counter");
    this.$level = $("#level");
    this.$highScore = $("#high-score");
    this.score = 0;
    this.level = 0;
    this.pause = true;
    this.gameOver = false;
    this.direction = 40; // starting direction
    this.board = new Board(20);
    this.snake = new Snake();
    this.food = new Food(this.board);
    this.setHighScore();

    $(document).keydown(this.eventHandler.bind(this));
  };

  Game.prototype.eventHandler = function (e) {
    var arrow_keys = [40, 39, 38, 37];

    var key_code = e.which;

    if( arrow_keys.indexOf(key_code) !== -1 ) {
      this.direction = key_code;
      return;
    }

    switch(key_code) {
      case 80: // 'p'
        this.pause = !this.pause;

        if ( !this.pause ) {
          this.nextMove();
        }
        break;
      case 83: //'s'
        if ( this.pause ) {
          this.pause = !this.pause;
          this.nextMove();

        } else if ( this.gameOver ) {
          this.restart();
        }
        break;
      case 70: // 'f'
        this.snake.increaseSpeed();
        break;
      case 82: // 'r'
        this.snake.resetSpeed();
        break;
      default:
        return;
    }
  };

  Game.prototype.restart = function() {
    $("#previous-score").text(this.score);
    SnakeGame.createNewGame();
  };

  Game.prototype.render = function () {
    $(".cell").remove();

    this.board.renderGrid(this.$el);
    SnakeGame.getjQueryObj(this.snake.row, this.snake.column).addClass("snake");
    SnakeGame.getjQueryObj(this.food.row, this.food.column).addClass("food");
    $(".instructions").text("Press 's' to start. Press 'p' to pause. Use arrow keys to change direction.");
    $("#counter").text(this.score);
  };

  Game.prototype.endGameProcedure = function () {
    $(".cell").addClass("gameOver");

    if (this.score > this.highScore) {
        this.highScore = this.score;
        docCookies.setItem('snake-high-score', this.highScore.toString());
    }

    this.$instructions.text("Game Over! Press 's' to restart.");
  };

  Game.prototype.checkSpeed = function () {
    if (this.score % 5 === 0) {
      this.snake.increaseSpeed();
      this.updateLevel();
    }
  };

  Game.prototype.updateLevel = function () {
    this.level++;
    this.$level.text(this.level);
  };

  Game.prototype.updateScore = function () {
    this.$counter.text(++this.score);
  };

  Game.prototype.growSnake = function () {
    SnakeGame.getjQueryObj(this.snake.row, this.snake.column).addClass("snake");
    SnakeGame.getjQueryObj(this.snake.row, this.snake.column).removeClass("food");
    this.checkSpeed();
    this.food.generate();

    SnakeGame.getjQueryObj(this.food.row, this.food.column).addClass("food");
  };

  Game.prototype.update = function () {
    this.snake.move(this.direction);

    // if off the grid or hits itself
    if ( this.snake.row < 0 || this.snake.row > 19 ||
        this.snake.column < 0 || this.snake.column > 19 ||
          SnakeGame.getjQueryObj(this.snake.row, this.snake.column).hasClass("snake")) {
          this.gameOver = true;
          this.endGameProcedure();
        }

    if ( this.snake.row === this.food.row &&
          this.snake.column === this.food.column ) {
            this.growSnake();
            this.updateScore();
    } else {
      SnakeGame.getjQueryObj(this.snake.row, this.snake.column).addClass("snake");

      var tail = this.snake.body.pop();
      SnakeGame.getjQueryObj(tail[0], tail[1]).removeClass("snake");
    }
  };

  Game.prototype.nextMove = function () {
    var game = this;
    setTimeout( function () {
      game.update();

      if( !game.pause && !game.gameOver ) {
        game.nextMove();
      }
    }, game.snake.speed);
  };

  Game.prototype.setHighScore = function() {
    this.highScore = this.highScore || 0;
    var cookie = docCookies.getItem('snake-high-score');

    if (cookie) { this.highScore = parseInt(cookie); }

    this.$highScore.text(this.highScore);
  };

  SnakeGame.createNewGame = function() {
    $(document).off('keydown');

    var $el = $(".wrapper");
    $(".cell").remove();
    $("#level").text(1);
    $el.empty();
    var game = new Game($el);
    game.render();
  };

  SnakeGame.getjQueryObj = function(row, col) {
    return $("#c_" + row + "_" + col);
  };
})();

$(document).ready(function () {
  SnakeGame.createNewGame();
});
